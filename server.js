const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.use(bodyParser.json({ limit: '10mb' }));
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/log', (req, res) => {
  const data = {
    src: req.query.src
  };
  res.render('log', {data});
});


app.post('/log', (req, res) => {
  
  const reqBody = req.body;
  let logData = {
    api:reqBody.api,
    url : reqBody.url,
    response : reqBody.response
  };
  var eventName = "logData_" + String(req.query.src)
  io.emit(eventName, logData);
  res.sendStatus(200)  
});


io.on('connection', (socket) => {
  console.log('An user connected');
});

http.listen(process.env.PORT || 5000, () => {
  console.log('Server is running on port 3001');
});

